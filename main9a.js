class Account {
    constructor(number, balance, currency) {
        this.number = number;
        this.balance = balance;
        this.currency = currency;
    }
}
class Person {
    constructor(firstName, lastName, accountList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountList = accountList;
    };
    findAccount(accountNumber){
        return this.accountList.find(account => {
            return account.number === accountNumber});
    }
    _calculateBalance() {
        var sum = 0;
        for (let account of this.accountList) {
            sum += account.balance;
        }
        return sum;
    };
    withdraw(accountNumber, amount){
        return new Promise((resolve, reject) => {
            let personAccount = this.findAccount(accountNumber);
            if(personAccount && amount <= personAccount.balance) {
                personAccount.balance -= amount;
                setTimeout( () => {
                    resolve(`Konto ${accountNumber} nowy stan: ${personAccount.balance}, po wyplacie ${amount}`)},1000);
            } else {
                reject('Error contact your bank');
            }
        }
        )
    }
      
    sayHello() {
        return `Hi! Jestem  ${this.firstName}  ${this.lastName}, mam ${this.accountList.length} konta i na nich jest: ${this._calculateBalance()}`
    }
    filterPositiveAccounts() {
      return this.accountList.filter(konto => {
            return konto.balance > 0
      });
    }
    addAccount(account) {
        this.accountList.push(account);
    }
}