class Account {
    constructor(balance, currency, number) {
        this.balance = balance;
        this.currency = currency;
        this.number = number;
    }
}
class Person {
    constructor(firstName, lastName, accountList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountList = accountList;
    };
    findAccount(accountNumber){
        return this.accountList.find(account => {
            return account.number === accountNumber});
    }
    _calculateBalance() {
        var sum = 0;
        for (let account of this.accountList) {
            sum += account.balance;
        }
        return sum;
    };
    withdraw(accountNumber, amount){
        //let personAccount = this.findAccount(accountNumber);
        return new Promise((resolve, reject) => {
            let personAccount = this.findAccount(accountNumber);
            if(personAccount && amount <= personAccount.balance) {
                personAccount.balance -= amount;
                setTimeout( () => {
                    resolve(`Konto ${accountNumber} nowy stan: ${personAccount.balance}, po wyplacie ${amount}`)},3000);
            } else {
                reject('Error contact your bank');
            }
        }
        )
    }
    sayHello() {
        return `Hi! Jestem  ${this.firstName}  ${this.lastName}, mam ${this.accountList.length} konta i na nich jest: ${this._calculateBalance()}`
    }
    filterPositiveAccounts() {
      return this.accountList.filter(konto => {
            return konto.balance > 0
      });
    }
    addAccount(account) {
        this.accountList.push(account);
    }
}
var konto1 = new Account(1000,"PLN",4);
const person1 = new Person("John", "Rambo", [new Account(3000, "PLN",1), new Account(5000, "PLN",2), new Account(0, "PLN",3),konto1]);
var konto = new Account(1500,"PLN",5);
person1.addAccount(konto);
console.log(person1.findAccount(3));

person1.withdraw(1,1000)
    .then((success) => {
        console.log(success)
    }
    ).catch((fail) =>{
        console.log(fail)
    }
    )
person1.withdraw(3,5000)
    .then((success) => {
        console.log(success)
    }
    ).catch((fail) =>{
        console.log(fail)
    }
    )
