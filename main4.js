class Account {
    constructor(balance, currency) {
        this.balance = balance;
        this.currency = currency;
    }
}
class Person {
    constructor(firstName, lastName, accountList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountList = accountList;
    };
    _calculateBalance() {
        var sum = 0;
        for (let account of this.accountList) {
            sum += account.balance;
        }
        return sum;
    };
    sayHello() {
        return `Hi! Jestem  ${this.firstName}  ${this.lastName}, mam ${this.accountList.length} konta i na nich jest: ${this._calculateBalance()}`
    }
    filterPositiveAccounts() {
      return this.accountList.filter(konto => {
            return konto.balance > 0
      });
    }
    addAccount(account) {
        this.accountList.push(account);
    }
}
const person1 = new Person("John", "Rambo", [new Account(2000, "PLN"), new Account(5000, "PLN")]);
console.log(person1.sayHello());
person1.addAccount(new Account(0, "?"));
console.log(person1.sayHello());
console.log(person1.filterPositiveAccounts());
console.log(person1.accountList);