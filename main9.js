const  init = ( () => {
    document.addEventListener("DOMContentLoaded", function() {
    const person1 = new Person("John", "Rambo", [new Account(1,3000, "PLN"), new Account(2,5000, "PLN")]);
    const personFullName = document.querySelector('.card-title');
    const personAccounts = document.querySelector('.card-text');
    const idInput = document.querySelector("#number");
    const amountInput = document.querySelector("#amount");
    const withdrawButton = document.querySelector(".btn.btn-primary");

    personFullName.innerHTML = person1.firstName+' '+person1.lastName;

    function start(){
        var text="";
    for(var i = 0; i<person1.accountList.length;i++){
        text += "<p>Konto nr " +person1.accountList[i].number + "<br>" + "Dostepne środki: "
                +person1.accountList[i].balance +" "+ person1.accountList[i].currency +"</p>";
                personAccounts.innerHTML = text;
    }

    }
    function onClicked(){
        let accNumber = parseFloat(idInput.value);
        let amount = parseFloat(amountInput.value);

        if(amount>0 && !isNaN(amountInput.value)){
            person1.withdraw(accNumber,amount)
            .then((success) => {
                console.log(success)
                document.querySelector('#error').innerHTML = "Wypłaciłeś "+amount+" PLN, aktualny stan konta wynosi: "+ person1.accountList[accNumber-1].balance+ " PLN";
                document.getElementById("error").className = "alert alert-success";
                start();               
            }
            ).catch((fail) =>{
                console.log(fail)
                document.querySelector('#error').innerHTML = "Nie masz wystarczających środków lub konto nie istnieje";
                document.getElementById("error").className = "alert alert-danger";
            }
            )
            }
            else if(isNaN(amountInput.value)){
                document.querySelector('#error').innerHTML = "Został wprowadzony tekst";
                document.getElementById("error").className = "alert alert-danger";
            }
            else{
                document.querySelector('#error').innerHTML = "Kwota nie może być równa zero lub mniejsza od zera";
                document.getElementById("error").className = "alert alert-danger";
            }
    }

    window.onload = start();
    withdrawButton.addEventListener("click", onClicked);        
    withdrawButton.setAttribute('disabled','disabled');

    function onChanged() {
            let idNumber = idInput.value;
            let amount = amountInput.value;
    
            if (idNumber && amount) {
                withdrawButton.disabled = false;
            } else {
                withdrawButton.disabled = true;
            }
        }
    
        idInput.addEventListener("change", onChanged);
        amountInput.addEventListener("change", onChanged);
        idInput.addEventListener("keyup", onChanged);
        amountInput.addEventListener("keyup", onChanged);
    })
})();



