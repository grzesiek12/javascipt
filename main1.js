var personFactory = function () {
    var details = {
        firstName: 'John',
        lastName: 'Rambo',
        accountList: [
            {
            balance: 2000,
            currency: "PLN"
            },
            {
            balance: 4000,
            currency: "PLN"
            }
            ]

    };

    return {
        firstName: details.firstName,
        lastName: details.firstName,
        sayHello: function () {
            return "Hello! " + this.firstName + " " + this.lastName +" mam " + details.accountList.length + " konta";
        }
    }
};


var person = personFactory();
console.log(person.sayHello());